import template from './home.component.html';
import controller from './home.controller';

export const HomeComponent = {
    template,
    controller
};
