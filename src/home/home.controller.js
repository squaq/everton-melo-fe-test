import moment from 'moment';

export default function HomeController ($scope, $state, shareData) {
    'ngInject';
    
    $scope.retDisabled = true;
    $scope.oneWayDisabled = '';
    $scope.endVisible = '';

    $scope.oneWayReturnBtClick = () => {
        if ($scope.retDisabled) {
            $scope.retDisabled = false;
            $scope.oneWayDisabled = true;
            $scope.endVisible = 'is-hidden';
        } else {
            $scope.retDisabled = true;
            $scope.oneWayDisabled = false;
            $scope.endVisible = '';
        }
    };

    $scope.disabled = () => {
        return !(shareData.endDate && shareData.startDate && shareData.flyFrom && shareData.flyTo);
    };

    $scope.searchFlyght = () => {
        $state.go('home.flightlist', { from: shareData.flyFrom.iataCode, fromname: shareData.flyFrom.name, fromcountry: shareData.flyFrom.country.name, to: shareData.flyTo.iataCode, toname: shareData.flyTo.name, tocountry: shareData.flyTo.country.name, leave: moment(shareData.startDate).format('YYYY-MM-DD'), return: moment(shareData.endDate).format('YYYY-MM-DD') });
    };
}
