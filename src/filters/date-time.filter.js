/*using the moment.js as a filter to dates and time
* https://momentjs.com/
*/

import moment from 'moment';

export function DateTimeFilter() {
    'ngInject';
    
    return (dateString, format)=>{
        return moment(dateString).format(format);    
    }
}