import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bootstrap from 'angular-ui-bootstrap';
import Components from './components/components';
import { HomeComponent } from './home/home.component';
import { FlightsComponent } from './flights/flights.component';
import {
    CheapFlightService,
    AirportsService
} from './services';
import { ShareDataFactory } from './factories';
import { DateTimeFilter} from './filters';
import './scss/main.scss';

angular.module('myApp', [
    uiRouter,
    bootstrap,
    Components
])
.component('homePage', HomeComponent)
.component('flightsPage', FlightsComponent)
.filter('dateTimeFilter', DateTimeFilter)
.service('AirportsService', ['$http', AirportsService])
.service('CheapFlightService',['$http', CheapFlightService])
.factory('shareData', () => new ShareDataFactory)
.config(($stateProvider) => {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '',
      template: '<home-page></home-page>'
    })
    .state('home.flightlist',{
      url:'/flightlist?from&fromname&fromcountry&to&toname&tocountry&leave&return',
      template: '<flights-page></flights-page>'
    })
});
