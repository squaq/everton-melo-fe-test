import template from './flights.component.html';
import controller from './flights.controller';
import './flights.component.scss';

export const FlightsComponent = {
    template,
    controller
};
