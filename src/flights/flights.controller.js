import moment from 'moment';
export default function FlightsController($scope, $stateParams, $anchorScroll, CheapFlightService ) {
  'ngInject';
    
    $scope.flights = null;
    $scope.$infos = $stateParams;
    $anchorScroll('flightsList');
    $scope.$showerror = 'is-hidden';
    $scope.$hideloading = '';
    
    CheapFlightService($stateParams.from, $stateParams.to, $stateParams.leave, $stateParams.return,(s)=>{
            $scope.flights = s.data.flights;
            $scope.$hideloading = 'is-hidden';
        }, (e)=>{
            $scope.$showerror = '';
            $scope.$hideloading = 'is-hidden';
        });
}
