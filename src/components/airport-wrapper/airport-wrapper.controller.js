
export default function AirportWrapperController($scope, AirportsService, shareData) {
    'ngInject';
    $scope.$ctrl.flylist = null;
    $scope.$showerror = 'is-hidden';
    $scope.$loading='is-loading is-disabled'
    
    AirportsService.then((s)=>{
        $scope.$ctrl.flylist = s.data;
        $scope.$loading = '';

    }, (e)=>{
        console.log('error', e);
        $scope.$ctrl.flylist = 'error';
        $scope.$loading = '';
        $scope.$showerror = '';
    })

    $scope.$watch('$ctrl.flyFrom', () => {
        if(this.flyFrom == this.flyTo){
            this.flyFrom = null;
        }
        shareData.flyFrom = this.flyFrom;
    });

    $scope.$watch('$ctrl.flyTo', () => {
         if(this.flyTo == this.flyFrom){
            this.flyTo = null;
        }
        shareData.flyTo = this.flyTo;
    });
    
    
}
