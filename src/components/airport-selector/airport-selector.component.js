import template from './airport-selector.component.html';
import controller from './airport-selector.controller';

export const AirportSelectorComponent = {
    bindings:{
        airport: '=',
        flylist: '=',
        placelabel:'=',
    },
    template,
    controller
};