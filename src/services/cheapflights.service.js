export const CheapFlightService = ($http) => {
    return ( from, to, startDate, endDate, success, error ) => {
        $http.get( 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/from/' + from + '/to/' + to + '/' + startDate + '/' + endDate + '/250/unique/?limit=15&offset-0' ).then((s) => {
            success(s);
        }, (e) => {
            error(e);
        });
    };
};
